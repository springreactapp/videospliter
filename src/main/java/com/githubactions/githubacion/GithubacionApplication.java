package com.githubactions.githubacion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GithubacionApplication {

	public static void main(String[] args) {
		SpringApplication.run(GithubacionApplication.class, args);
	}

}
